// vfont_extractor version 0.1
// Copyright (c) 2020 Noah Olson. See LICENSE file in root source directory.
// works on VFONT version 1

#include <stdio.h>
#include <string.h>

unsigned char MAGIC = 167;

int main(int argc, char* argv[])
{
	FILE* in_file = fopen(argv[1], "rb");
	if (!in_file)
	{
		printf("Failed to open input file");
		return 1;
	}
	FILE* out_file = fopen(argv[2], "wb");
	if (!out_file)
	{
		printf("Failed to open output file");
		return 1;
	}

	fseek(in_file, -(int)(strlen("VFONT1") + 1), SEEK_END);
	unsigned char num_magic_bytes = getc(in_file);
	size_t data_length = ftell(in_file) - num_magic_bytes;
	int magic = MAGIC;
	fseek(in_file, -num_magic_bytes, SEEK_CUR);
	num_magic_bytes--;
	for (int i = 0; i < num_magic_bytes; i++)
	{
		unsigned char byte = getc(in_file);
		magic ^= (byte + MAGIC) % 256;
	}

	rewind(in_file);
	for (int i = 0; i < data_length; i++)
	{
		unsigned char byte = getc(in_file);
		putc(byte ^ magic, out_file);
		magic = (byte + MAGIC) % 256;
		
	}

	fclose(in_file);
	fclose(out_file);
}