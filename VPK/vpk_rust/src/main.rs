use std::env;
use std::vec::Vec;
use colour;
use byteorder::{ByteOrder, LittleEndian};
// use std::io::Read;
use pak_lib::{FileEntry, DirectoryEntry, Node, Package};
use std::io::{Seek, SeekFrom, Read};

const DIRECTORY_NO_MAGIC: &str = "Not a valid dir.vpk file; missing magic";
const DIRECTORY_EOF : &str = "Reached EOF while reading dir.vpk";
const DIRECTORY_OPEN : &str = "Failed to open dir.vpk file";
const DIRECTORY_NAME_UNRECOGNIZED : &str = "Unrecognized dir.vpk file name; unable to find corresponding vpk files";
const FILE_DECOMPRESS : &str = "Failed to decompress file";
const TEMP_CREATE : &str = "Failed to create temporary file";
const TEMP_CLEANUP : &str = "Failed to cleanup temporary files";
const VPK_OPEN : &str = "Failed to open raw vpk file";
const PATH_FILE_WITHIN_FILE : &str = "Requested path is a file within a file";
const PATH_DOES_NOT_EXIST : &str = "Requested path does not exist in vpk";
const UNPACK_DIRECTORY_CREATE : &str = "Failed to create output directory";

#[derive(Debug)]
struct VPKError<'a>(&'a str);

impl std::fmt::Display for VPKError<'_>
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result
    {
        write!(f, "{}", self.0)
    }
}

impl std::error::Error for VPKError<'_>{}


fn print_error(error_message: &str)
{
    colour::red_ln!(error_message);
}

fn print_warning(warning_message: &str)
{
    colour::yellow_ln!(warning_message);
}

fn print_success(success_message: &str)
{
    colour::green_ln!(success_message);
}

fn print_accent(text: &str)
{
    colour::cyan_ln!(text);
}


mod vpk_const {
    pub const DIRECTORY_MAGIC: [u8; 4] = [0x34, 0x12, 0xAA, 0x55];
    pub const CURRENT_MAJOR_VERSION: u16 = 2;
    pub const CURRENT_MINOR_VERSION: u16 = 3;
}

const LZHAM_EXECUTABLE_NAME: &str = "lzham_exe";
const TEMP_FILE_NAME: &str = "temp";
const RESULT_FILE_NAME: &str = "result";
#[cfg(target_os = "windows")]
const LZHAM_EXECUTABLE: &[u8] = include_bytes!("lzham_windows.exe");
#[cfg(target_os = "linux")]
const LZHAM_EXECUTABLE: &[u8] = include_bytes!("lzham_linux_exe");


struct VPKPackage {
    path: String,
    num_archives: u8,
    directory: VPKDirectoryEntry,
}

impl Package for VPKPackage {
    type D = VPKDirectoryEntry;

    fn get_file_path(&self) -> &str {
        return &self.path;
    }

    fn get_directory(&self) -> &Self::D {
        return &self.directory;
    }

    fn load(directory_path: String) -> std::io::Result<Self> {
        let directory_contents = std::fs::read(&directory_path)?;
        if directory_contents.len() < 4+2+2+4+4+3 {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "directory is too short",
            ));
        }

        let mut consumed = 0;

        if directory_contents[..vpk_const::DIRECTORY_MAGIC.len()] != vpk_const::DIRECTORY_MAGIC {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "missing magic",
            ));
        }
        consumed += vpk_const::DIRECTORY_MAGIC.len();

        if LittleEndian::read_u16(&directory_contents[consumed..consumed + 2]) != vpk_const::CURRENT_MAJOR_VERSION ||
        LittleEndian::read_u16(&directory_contents[consumed + 2..consumed + 4]) != vpk_const::CURRENT_MINOR_VERSION {
            println!("warning: vpk version is unrecognized; it may fail to load");
        }
        consumed += 4;

        // skip the directory_length and pad
        consumed += 8;

        let mut directory = VPKDirectoryEntry::new("root".to_string());
        let mut max_archives = 0;

        // for each extension
        while directory_contents[consumed] != 0x00 {
            let (str_len, extension) = read_string(&directory_contents[consumed..])?;
            consumed += str_len;

            // for each path
            while directory_contents[consumed] != 0x00 {
                let (str_len, path) = read_string(&directory_contents[consumed..])?;
                consumed += str_len;

                let parts: Vec<_> = path.split('/').collect();
                let parent = directory.find_or_create_path(&parts);

                // for each file
                while directory_contents[consumed] != 0x00 {
                    let (str_len, mut file_name) = read_string(&directory_contents[consumed..])?;
                    consumed += str_len;
                    file_name += ".";
                    file_name += extension.as_str();

                    let crc = LittleEndian::read_u32(&directory_contents[consumed..consumed + 4]);
                    consumed += 4;

                    let mut file_parts = Vec::new();
                    let mut file_total_compressed = 0;
                    let mut file_total_uncompressed = 0;

                    // for each file part
                    while directory_contents[consumed..consumed + 4] != [0x00, 0x00, 0xFF, 0xFF] {
                        let preload_bytes = LittleEndian::read_u16(&directory_contents[consumed..consumed + 2]);
                        consumed += 2;

                        let archive_index = LittleEndian::read_u16(&directory_contents[consumed..consumed + 2]);
                        consumed += 2;
                        if archive_index > max_archives {
                            max_archives = archive_index;
                        }

                        // skip pad
                        consumed += 6;

                        let offset = LittleEndian::read_u32(&directory_contents[consumed..consumed + 4]);
                        consumed += 4;

                        // skip pad
                        consumed += 4;

                        let compressed_length = LittleEndian::read_u32(&directory_contents[consumed..consumed + 4]);
                        consumed += 4;
                        file_total_compressed += compressed_length;

                        // skip pad
                        consumed += 4;

                        let uncompressed_length = LittleEndian::read_u32(&directory_contents[consumed..consumed + 4]);
                        consumed += 4;
                        file_total_uncompressed += uncompressed_length;

                        // skip pad
                        consumed += 2;

                        file_parts.push(FilePart {
                            preload_bytes,
                            archive_index,
                            offset,
                            compressed_length,
                            uncompressed_length,
                        });
                    }

                    // parts terminator
                    consumed += 4;

                    parent.children.push(Node::File(VPKFileEntry {
                        name: file_name,
                        crc,
                        file_parts,
                        compressed_size: file_total_compressed as u64,
                        uncompressed_size: file_total_uncompressed as u64,
                    }));
                }

                // files terminator
                consumed += 1;
            }

            // paths terminator
            consumed += 1;
        }

        return Ok(VPKPackage {
            path: directory_path,
            num_archives: max_archives as u8 + 1,
            directory
        });
        unimplemented!()
    }

    fn unpack(&self, output_directory: &str) -> std::io::Result<()> {
        let mut working_dir = std::path::PathBuf::from(std::env::args().next().unwrap());
        working_dir.pop();
        working_dir.push(LZHAM_EXECUTABLE_NAME);
        let executable_path = working_dir.to_str().unwrap().to_string();
        std::fs::write(executable_path, LZHAM_EXECUTABLE);
        let mut archive_files = open_archives(&self.path, self.num_archives)?;
        let mut unpack_state = UnpackState { archive_files };
        if std::path::Path::new(output_directory).exists() {
            std::fs::remove_dir_all(output_directory)?;
        }
        return self.directory.unpack(output_directory, &mut unpack_state);

        // cleanup_temporary_files()?;
    }

    fn pack(directory: &str) -> std::io::Result<()> {
        unimplemented!()
    }
}

struct VPKFileEntry
{
    pub name: String,
    pub crc: u32,
    pub file_parts: Vec<FilePart>,
    // pub file_ext: String,
    pub compressed_size: u64,
    pub uncompressed_size: u64,
}

struct FilePart
{
    pub preload_bytes: u16,
    pub archive_index: u16,
    pub offset: u32,
    pub compressed_length: u32,
    pub uncompressed_length: u32,
}

impl FileEntry for VPKFileEntry {
    type UnpackState = UnpackState;

    fn get_file_name(&self) -> &str {
        return &self.name;
    }

    fn get_archive_index(&self) -> u8 {
        return self.file_parts[0].archive_index as u8;
    }

    fn get_offset(&self) -> u64 {
        return self.file_parts[0].offset as u64;
    }

    fn get_compressed_size(&self) -> Option<u64> {
        return Some(self.compressed_size);
    }

    fn get_uncompressed_size(&self) -> Option<u64> {
        return Some(self.uncompressed_size);
    }

    fn unpack(&self, base_directory: &str, unpack_state: &mut Self::UnpackState) -> std::io::Result<()> {
        // write each chunk to temp file
        // run lzham
        // mv file to its name

        let mut temp_buffer = Vec::new();

        let mut parts_buf = [0u8; 4];
        LittleEndian::write_u32(&mut parts_buf, self.file_parts.len() as u32);
        temp_buffer.extend_from_slice(&parts_buf);

        for part in &self.file_parts {
            let mut length_buf = [0u8; 4];
            if part.compressed_length == part.uncompressed_length {
                LittleEndian::write_u32(&mut length_buf, 0);
                temp_buffer.extend_from_slice(&length_buf);
            }
            LittleEndian::write_u32(&mut length_buf, part.compressed_length);
            temp_buffer.extend_from_slice(&length_buf);

            let mut archive = &unpack_state.archive_files[part.archive_index as usize];
            archive.seek(SeekFrom::Start(part.offset as u64))?;
            let write_base = temp_buffer.len();
            temp_buffer.resize(write_base + part.compressed_length as usize, 0);
            archive.read_exact(&mut temp_buffer.as_mut_slice()[write_base..])?;
        }

        let mut crc_buf = [0u8; 4];
        LittleEndian::write_u32(&mut crc_buf, self.crc);
        temp_buffer.extend_from_slice(&crc_buf);

        let mut working_dir = std::path::PathBuf::from(std::env::args().next().unwrap());
        working_dir.pop();
        working_dir.push(TEMP_FILE_NAME);
        let temp_file_path = working_dir.to_str().unwrap().to_string();
        working_dir.pop();
        working_dir.push(LZHAM_EXECUTABLE_NAME);
        let executable_path = working_dir.to_str().unwrap().to_string();
        working_dir.pop();
        working_dir.push(RESULT_FILE_NAME);
        let result_file_path = working_dir.to_str().unwrap().to_string();

        std::fs::write(&temp_file_path,temp_buffer.as_slice())?;

        if !std::process::Command::new(&executable_path)
            .args(&["d", &temp_file_path, &result_file_path])
            .status()
            .unwrap()
            .success()
        {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "lzham failed",
            ));
        }

        let mut file_path = std::path::PathBuf::from(base_directory);
        file_path.push(&self.name);
        std::fs::copy(&result_file_path, file_path.to_str().unwrap())?;
        std::fs::remove_file(&result_file_path)?;
        // std::fs::rename(result_file_path, file_path.to_str().unwrap())?;

        return Ok(());
    }
}

struct VPKDirectoryEntry
{
    pub is_vpk_root: bool,
    pub is_root: bool,
    pub name: String,
    pub children: Vec<Node<Self>>,
}

impl DirectoryEntry for VPKDirectoryEntry {
    type F = VPKFileEntry;

    fn new(name: String) -> Self {
        return Self {
            is_vpk_root: false,
            is_root: false,
            name,
            children: Vec::new(),
        };
    }

    fn get_directory_name(&self) -> &str {
        return &self.name;
    }

    fn get_children(&self) -> &Vec<Node<Self>> {
        return &self.children;
    }

    fn get_children_mut(&mut self) -> &mut Vec<Node<Self>> {
        return &mut self.children;
    }
}

struct UnpackState {
    archive_files: Vec<std::fs::File>,
}

fn read_string(buf: &[u8]) -> std::io::Result<(usize, String)> {
    let mut consumed = 0;
    while buf[consumed] != 0x00 {
        consumed += 1;
    }
    let string = String::from_utf8(Vec::from(&buf[..consumed])).unwrap();
    return Ok((consumed + 1, string));
}

fn open_archives(dir_file_path: &str, num_archives: u8) -> std::io::Result<Vec<std::fs::File>> {
    let dir_file = std::path::Path::new(dir_file_path);
    let directory = dir_file.parent().unwrap();
    let file = dir_file.file_name().unwrap().to_str().unwrap();
    let unlocalized_name = match file.find("client") {
        Some(begin) => &file[begin..],
        None => return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            "vpk name doesn't contain \"client\"",
        )),
    };
    let archive_base_name = &unlocalized_name[..unlocalized_name.len() - "dir.vpk".len()];

    let mut archive_base_path = std::path::PathBuf::from(directory);
    archive_base_path.push(archive_base_name);
    let archive_base_path = archive_base_path.to_str().unwrap();

    let mut archives = Vec::new();
    for i in 0..num_archives {
        let mut current_archive_name = archive_base_path.to_string();
        let archive_index = i.to_string();
        current_archive_name += "0".repeat(3 - archive_index.len()).as_str();
        current_archive_name += archive_index.as_str();
        current_archive_name += ".vpk";
        archives.push(std::fs::File::open(current_archive_name)?);
    }

    return Ok(archives);
}

fn cleanup_temporary_files() -> std::io::Result<()> {
    unimplemented!()
}

fn main()
{
    pak_lib::cli::run_cli::<VPKPackage>();
}
