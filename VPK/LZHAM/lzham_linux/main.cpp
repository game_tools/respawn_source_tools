// LZHAM compression and decompression binary with static dictionary size
// and compression setting for Respawn vpks.
// Copyright (c) 2020 Noah Olson. See LICENSE file in root source directory.
//
// LZHAM Copyright (c) 2009-2011 Richard Geldreich, Jr. <richgel99@gmail.com>
//
// Link against lzhamlib_x64.lib, lzhamcomp_x64.lib, and lzhamdecomp_x64.lib.
// If you need to build lzham, build with VS - the CMake files do not work.


#include <stdio.h>
#include <cstring>
#include<stdbool.h>
#include "lzham.h"
#include "lzham_decomp.h"
#include "lzham_comp.h"

typedef unsigned char uint8;
typedef unsigned int uint;
typedef unsigned int uint32;

#define RESPAWN_DICT_SIZE 20U
#define RESPAWN_MAX_CHUNK_SIZE 1048576

#define IN_BUFFER_SIZE 65536*4
#define OUT_BUFFER_SIZE 65536*4

#define MIN(num_1, num_2) ((num_1) < (num_2) ? (num_1) : (num_2))


const uint cInitCRC32 = 0U;

static const lzham_uint32 s_crc32[16] =
{
   0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac, 0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
   0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c, 0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
};

uint crc32(uint crc, const lzham_uint8* ptr, size_t buf_len)
{
    if (!ptr)
        return cInitCRC32;

    crc = ~crc;
    while (buf_len--)
    {
        lzham_uint8 b = *ptr++;
        crc = (crc >> 4) ^ s_crc32[(crc & 0xF) ^ (b & 0xF)];
        crc = (crc >> 4) ^ s_crc32[(crc & 0xF) ^ (b >> 4)];
    }
    return ~crc;
}


int main(int argc, char* argv[])
{
    // parse command line
    if (argc != 4)
    {
        printf("LZHAM: Incorrect number of arguments\n");
        return 1;
    }
    if (!(!strcmp(argv[1], "d") || !strcmp(argv[1], "c")))
    {
        printf("LZHAM: Invalid operation\n");
        return 1;
    }
    // open files
    FILE* in_file = fopen(argv[2], "rb");
    if (!in_file)
    {
        printf("LZHAM: Failed to open input file\n");
        return 1;
    }
    FILE* out_file = fopen(argv[3], "wb");
    if (!out_file)
    {
        printf("LZHAM: Failed to open output file\n");
        return 1;
    }


    // will shrink when end of input file reached
    size_t in_buf_current_size = IN_BUFFER_SIZE;
    // this doesn't change
    size_t out_buf_current_size = OUT_BUFFER_SIZE;

    // make sure buffer gets filled on first run
    size_t in_buf_offset = in_buf_current_size;
    size_t out_buf_offset = 0;

    // allocate buffers
    const uint8* in_buf_base = malloc(in_buf_current_size);
    const uint8* out_buf_base = malloc(out_buf_current_size);
    if (!in_buf_base || !out_buf_base)
    {
        printf("LZHAM: Failed to allocate buffers\n");
        return 1;
    }


    // compress or decompress
    if (!strcmp(argv[1], "d"))
    {
        // initialized decompressor
        lzham_decompress_params params;
        params.m_struct_size = sizeof(lzham_decompress_params);
        params.m_dict_size_log2 = RESPAWN_DICT_SIZE;
        params.m_decompress_flags = 0U;
        params.m_num_seed_bytes = 0U;
        params.m_pSeed_bytes = NULL;

        lzham_decompress_state_ptr state = lzham::lzham_lib_decompress_init(&params);
        if (!state)
        {
            printf("LZHAM: Failed to initialize decompressor\n");
            return 1;
        }

        // initialize CRC
        unsigned int crc = cInitCRC32;

        // read number of chunks
        unsigned int in_file_chunks_left;
        if (fread(&in_file_chunks_left, 4, 1, in_file) != 1)
        {
            printf("LZHAM: Failed to read number of chunks\n");
            return 1;
        }

        // chunk loop
        for (; in_file_chunks_left; in_file_chunks_left--)
        {
            // keep track of how much data is left
            unsigned int bytes_left;
            if (fread(&bytes_left, 4, 1, in_file) != 1)
            {
                printf("LZHAM: Failed to read chunk size\n");
                return 1;
            }

            // 0 length chunk represents uncompressed chunk
            // write it straight to output
            if (!bytes_left)
            {
                if (fread(&bytes_left, 4, 1, in_file) != 1)
                {
                    printf("LZHAM: Failed to read uncompressed chunk size\n");
                    return 1;
                }
                while (bytes_left)
                {
                    if (fread(in_buf_base, 1, MIN(bytes_left, in_buf_current_size), in_file) != MIN(bytes_left, in_buf_current_size))
                    {
                        printf("LZHAM: Failed to read uncompressed chunk\n");
                        return 1;
                    }
                    if (fwrite(in_buf_base, 1, MIN(bytes_left, in_buf_current_size), out_file) != MIN(bytes_left, in_buf_current_size))
                    {
                        printf("LZHAM: Failed to write uncompressed chunk\n");
                        return 1;
                    }

                    crc = crc32(crc, in_buf_base, MIN(bytes_left, in_buf_current_size));

                    bytes_left -= MIN(bytes_left, in_buf_current_size);
                }
            }
            else
            {
                // reset buffers
                in_buf_current_size = IN_BUFFER_SIZE;
                in_buf_offset = in_buf_current_size;
                out_buf_offset = 0;

                // reset decompressor
                state = lzham::lzham_lib_decompress_reinit(state, &params);

                // decompression loop
                while (true)
                {
                    // refill input buffer if empty
                    if (in_buf_offset == in_buf_current_size)
                    {
                        size_t to_read = MIN(bytes_left, in_buf_current_size);
                        size_t read = fread(in_buf_base, 1, to_read, in_file);
                        if (read != to_read)
                        {
                            printf("LZHAM: Failed to read from input file\n");
                            return 1;
                        }

                        bytes_left -= to_read;
                        // shrinks the buffer size at end of chunk
                        in_buf_current_size = to_read;
                        in_buf_offset = 0;
                    }

                    // do decompression run
                    uint8* tmp_in_buf_ptr = in_buf_base + in_buf_offset;
                    size_t tmp_in_buf_size = in_buf_current_size - in_buf_offset;
                    uint8* tmp_out_buf_ptr = out_buf_base + out_buf_offset;
                    size_t tmp_out_buf_size = out_buf_current_size - out_buf_offset;

                    lzham_decompress_status_t status = lzham::lzham_lib_decompress(state, tmp_in_buf_ptr, &tmp_in_buf_size, tmp_out_buf_ptr, &tmp_out_buf_size, !bytes_left);

                    // adjust buffer offsets
                    in_buf_offset += tmp_in_buf_size;
                    out_buf_offset += tmp_out_buf_size;

                    // check success or failure
                    if (status > LZHAM_DECOMP_STATUS_SUCCESS)
                    {
                        printf("LZHAM: Failed to decompress with status %d\n", status);
                        return 1;
                    }
                    else if (status == LZHAM_DECOMP_STATUS_SUCCESS)
                    {
                        size_t written = fwrite(out_buf_base, 1, out_buf_offset, out_file);
                        if (written != out_buf_offset)
                        {
                            printf("LZHAM: Failed to write final buffer to file\n");
                            return 1;
                        }

                        crc = crc32(crc, out_buf_base, out_buf_offset);

                        break;
                    }

                    // write output buffer if full
                    if (out_buf_offset == out_buf_current_size)
                    {
                        size_t written = fwrite(out_buf_base, 1, out_buf_current_size, out_file);
                        if (written != out_buf_current_size)
                        {
                            printf("LZHAM: Failed to write output buffer to file\n");
                            return 1;
                        }

                        crc = crc32(crc, out_buf_base, out_buf_current_size);

                        out_buf_offset = 0;
                    }
                }
            }
        }

        // check CRC
        unsigned int expected_crc;
        if (fread(&expected_crc, 4, 1, in_file) != 1)
        {
            printf("LZHAM: Failed to read expected CRC\n");
            return 1;
        }
        if (crc != expected_crc)
        {
            printf("LZHAM: File CRC32 did not match check CRC32\n");
            return 1;
        }
    }
    else if (!strcmp(argv[1], "c"))
    {
        // initialize compressor
        lzham_compress_params params;
        params.m_struct_size = sizeof(lzham_compress_params);
        params.m_dict_size_log2 = RESPAWN_DICT_SIZE;
        params.m_level = LZHAM_COMP_LEVEL_FASTEST;
        params.m_max_helper_threads = -1;
        params.m_cpucache_total_lines = 0;
        params.m_cpucache_line_size = 0;
        params.m_compress_flags = 0;
        params.m_num_seed_bytes = 0;
        params.m_pSeed_bytes = NULL;

        lzham_compress_state_ptr state = lzham::lzham_lib_compress_init(&params);
        if (!state)
        {
            printf("LZHAM: Failed to initialize compressor\n");
            return 1;
        }

        // initialize CRC
        unsigned int crc = cInitCRC32;

        // write number of chunks
        fseek(in_file, 0, SEEK_END);
        const long in_file_length = ftell(in_file);
        fseek(in_file, 0, SEEK_SET);
        unsigned int in_file_chunks_left = in_file_length / RESPAWN_MAX_CHUNK_SIZE + 1;
        if (fwrite(&in_file_chunks_left, 4, 1, out_file) != 1)
        {
            printf("LZHAM: Failed to write number of chunks\n");
            return 1;
        }

        // chunk loop
        for (; in_file_chunks_left; in_file_chunks_left--)
        {
            // keep track of how much data is left
            bool no_more_input = false;
            unsigned int chunk_bytes_left = (in_file_chunks_left == 1 ? in_file_length % RESPAWN_MAX_CHUNK_SIZE : RESPAWN_MAX_CHUNK_SIZE);

            // reset buffers
            in_buf_current_size = IN_BUFFER_SIZE;
            in_buf_offset = in_buf_current_size;
            out_buf_offset = 0;

            // reset compressor
            state = lzham::lzham_lib_compress_reinit(state);

            // skip 4 bytes to write compressed length later
            unsigned int compressed_length = 0;
            if (fwrite(&compressed_length, 4, 1, out_file) != 1)
            {
                printf("LZHAM: Failed to write placeholder compressed length\n");
                return 1;
            }

            // compression loop
            while (true)
            {
                // refill input buffer if empty
                if (in_buf_offset == in_buf_current_size)
                {
                    // shrink input buffer if this is last run of chunk
                    if (chunk_bytes_left < in_buf_current_size)
                    {
                        in_buf_current_size = chunk_bytes_left;
                        no_more_input = true;
                    }

                    // read the file
                    size_t read = fread(in_buf_base, 1, in_buf_current_size, in_file);
                    if (read != in_buf_current_size)
                    {
                        printf("LZHAM: Failed to read from input file\n");
                        return 1;
                    }

                    chunk_bytes_left -= read;
                    in_buf_offset = 0;
                    // do crc of new buffer
                    crc = crc32(crc, in_buf_base, in_buf_current_size);
                }


                // do compression run
                uint8* tmp_in_buf_ptr = in_buf_base + in_buf_offset;
                size_t tmp_in_buf_size = in_buf_current_size - in_buf_offset;
                uint8* tmp_out_buf_ptr = out_buf_base + out_buf_offset;
                size_t tmp_out_buf_size = out_buf_current_size - out_buf_offset;

                lzham_compress_status_t status = lzham::lzham_lib_compress(state, tmp_in_buf_ptr, &tmp_in_buf_size, tmp_out_buf_ptr, &tmp_out_buf_size, no_more_input);

                // adjust buffer offsets
                in_buf_offset += tmp_in_buf_size;
                out_buf_offset += tmp_out_buf_size;
                // increase compressed length
                compressed_length += tmp_out_buf_size;

                // check success or failure
                if (status > LZHAM_COMP_STATUS_SUCCESS)
                {
                    printf("LZHAM: Failed to compress with status %d\n", status);
                    return 1;
                }
                else if (status == LZHAM_COMP_STATUS_SUCCESS)
                {
                    size_t written = fwrite(out_buf_base, 1, out_buf_offset, out_file);
                    if (written != out_buf_offset)
                    {
                        printf("LZHAM: Failed to write final buffer to file\n");
                        return 1;
                    }
                    break;
                }

                // write output buffer if full
                if (out_buf_offset == out_buf_current_size)
                {
                    size_t written = fwrite(out_buf_base, 1, out_buf_current_size, out_file);
                    if (written != out_buf_current_size)
                    {
                        printf("LZHAM: Failed to write buffer to file\n");
                        return 1;
                    }
                    out_buf_offset = 0;
                }
            }

            // go back and write the compressed length
            fseek(out_file, -(long)(compressed_length + 4), SEEK_CUR);
            if (fwrite(&compressed_length, 4, 1, out_file) != 1)
            {
                printf("LZHAM: Failed to write compressed length\n");
                return 1;
            }
            fseek(out_file, compressed_length, SEEK_CUR);
        }

        // write CRC
        if (fwrite(&crc, 4, 1, out_file) != 1)
        {
            printf("LZHAM: Failed to write CRC\n");
            return 1;
        }
    }



    // cleanup
    free(in_buf_base);
    free(out_buf_base);
    if (fclose(in_file))
    {
        printf("LZHAM: Failed to close input file\n");
        return 1;
    }
    if (fclose(out_file))
    {
        printf("LZHAM: Failed to close output file\n");
        return 1;
    }
    return 0;
}
