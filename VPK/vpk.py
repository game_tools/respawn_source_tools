import argparse
from typing import List, Dict
import struct
import shutil
import os
import hashlib
import sys

try:
    from colorama import Fore, Style

    def print_error(error_message: str):
        print(Fore.RED + 'Error: ' + error_message + Fore.RESET)

    def print_warning(warning_message: str):
        print(Fore.RED + 'Warning: ' + warning_message + Fore.RESET)

    def print_success(success_message: str):
        print(Fore.GREEN + success_message + Fore.RESET)

    def print_accent(text: str):
        print(Fore.CYAN + text + Fore.RESET)
except:
    def print_error(error_message: str):
        print('Error: ' + error_message)

    def print_warning(warning_message: str):
        print('Warning: ' + warning_message)

    def print_success(success_message: str):
        print(success_message)

    def print_accent(text: str):
        print(text)
if os.name == 'nt':
    os.system('color')

SCRIPT_DIR = os.path.abspath(sys.path[0])
TEMP_FILE = os.path.join(SCRIPT_DIR, 'temp')
RESULT_FILE = os.path.join(SCRIPT_DIR, 'result')
if os.name == 'nt':
    LZHAM_EXE = os.path.join(SCRIPT_DIR, 'LZHAM', 'lzham_windows.exe')
else:
    LZHAM_EXE = os.path.join(SCRIPT_DIR, 'LZHAM', 'lzham_linux_exe')


class ERROR:
    DIRECTORY_NO_MAGIC = 'Not a valid dir.vpk file; missing magic'
    DIRECTORY_EOF = 'Reached EOF while reading dir.vpk'
    DIRECTORY_OPEN = 'Failed to open dir.vpk file'
    DIRECTORY_NAME_UNRECOGNIZED = 'Unrecognized dir.vpk file name; unable to find corresponding vpk files'
    FILE_DECOMPRESS = 'Failed to decompress file'
    TEMP_CREATE = 'Failed to create temporary file'
    TEMP_CLEANUP = 'Failed to cleanup temporary files'
    VPK_OPEN = 'Failed to open raw vpk file'
    PATH_FILE_WITHIN_FILE = 'Requested path is a file within a file'
    PATH_DOES_NOT_EXIST = 'Requested path does not exist in vpk'
    UNPACK_DIRECTORY_CREATE = 'Failed to create output directory'


########################################
# Directory ############################
########################################
class FilePart:
    def __init__(self):
        self.preload_bytes = -1
        self.archive_index = -1
        self.offset = -1
        self.compressed_length = -1
        self.uncompressed_length = -1


class TreeNode:
    def print(self, indent: str = '', child_indent: str = ''):
        pass

    def is_file(self) -> bool:
        pass

    def get_name(self) -> str:
        pass


class DirectoryNode(TreeNode):
    def __init__(self, name: str, is_directory_root: bool = False):
        self.is_directory_root = is_directory_root

        self.is_root = False
        if name == ' ':
            name = 'root'
            self.is_root = True
        self.name = name

        self.children: List[TreeNode] = []

    def print(self, indent: str = '', child_indent: str = ''):
        print(indent + Fore.CYAN + self.name + Fore.RESET)
        for child in self.children:
            if child == self.children[-1]:
                child.print(child_indent + '└─', child_indent + '  ')
            else:
                child.print(child_indent + '├─', child_indent + '│ ')

    def is_file(self) -> bool:
        return False

    def get_name(self) -> str:
        return self.name

    def to_flat_directory(self) -> 'DirectoryNode':
        flat_directory = DirectoryNode('root', True)
        for extension in self.children:
            for path in extension.children:
                for file in path.children:
                    flat_directory.children.append(file)
        return flat_directory

    def to_path_directory(self) -> 'DirectoryNode':
        def find_or_create_path(root_node: 'DirectoryNode', path: str) -> 'DirectoryNode':
            current_node = root_node
            for path_part in path.split('/'):
                next_node = None
                for node in current_node.children:
                    if node.get_name() == path_part:
                        next_node = node
                        break
                if next_node:
                    current_node = next_node
                else:
                    new_node = DirectoryNode(path_part)
                    current_node.children.append(new_node)
                    current_node = new_node
            return current_node

        path_directory = DirectoryNode('root', True)
        for extension in self.children:
            for path in extension.children:
                if path.is_root:
                    path_node = path_directory
                else:
                    path_node = find_or_create_path(path_directory, path.get_name())

                for file in path.children:
                    path_node.children.append(file)
        return path_directory


class FileNode(TreeNode):
    def __init__(self, name: str):
        self.name = name
        self.CRC = 0
        self.file_parts: List[FilePart] = []
        self.file_ext = ''

    ID_LENGTH = 6
    OFFSET_LENGTH = 15
    SIZE_LENGTH = 15
    CRC_LENGTH = 14
    STAT_LENGTH = ID_LENGTH + OFFSET_LENGTH + SIZE_LENGTH + CRC_LENGTH + 1
    CONSOLE_WIDTH = shutil.get_terminal_size()[0]

    def print(self, indent: str = '', child_indent: str = ''):
        print_string = indent + self.name + '.' + self.file_ext
        spacing = self.CONSOLE_WIDTH - (len(print_string) + self.STAT_LENGTH)
        if spacing < 0:
            spacing = 0

        first_offset = str(self.get_first_offset())
        compressed_length = str(self.get_compressed_length())

        print(print_string + ' ' * spacing,
              'id:', self.get_first_index(),
              'ofs:' + ' ' * (10 - len(first_offset)) + first_offset,
              'csz:' + ' ' * (10 - len(compressed_length)) + compressed_length,
              'CRC:', struct.pack('<I', self.CRC).hex())

    def is_file(self) -> bool:
        return True

    def get_name(self) -> str:
        return self.name + '.' + self.file_ext

    def get_first_offset(self) -> int:
        return self.file_parts[0].offset

    def get_first_index(self) -> int:
        return self.file_parts[0].archive_index

    def is_continuous(self) -> bool:
        if len(self.file_parts) > 1:
            first_index = self.get_first_index()
            for i in range(len(self.file_parts) - 1):
                if not (self.file_parts[i].offset + self.file_parts[i].compressed_length
                        == self.file_parts[i + 1].offset
                        and self.file_parts[i + 1].archive_index == first_index):
                    return False
        return True

    def get_compressed_length(self) -> int:
        length = 0
        for part in self.file_parts:
            length += part.compressed_length
        return length

    def get_decompressed_length(self) -> int:
        length = 0
        for part in self.file_parts:
            length += part.uncompressed_length
        return length


def load_vpk_directory(dir_vpk_file: str) -> DirectoryNode:
    def protected_read(size: int) -> bytes:
        data = dir_file.read(size)
        if len(data) < size:
            raise ValueError(ERROR.DIRECTORY_EOF)
        return data

    def protected_peek(size: int) -> bytes:
        data = protected_read(size)
        dir_file.seek(-size, 1)
        return data

    def read_string() -> str:
        string = ''
        while protected_peek(1)[0] != 0:
            string += protected_read(1).decode('ascii')
        dir_file.seek(1, 1)
        return string

    def read_uint() -> int:
        return int.from_bytes(protected_read(4), 'little', signed=False)

    def read_short() -> int:
        return int.from_bytes(protected_read(2), 'little', signed=True)

    def read_file_part() -> FilePart:
        file_part = FilePart()

        file_part.preload_bytes = read_short()
        file_part.archive_index = read_short()
        # padding
        dir_file.seek(6, 1)
        file_part.offset = read_uint()
        # padding
        dir_file.seek(4, 1)
        file_part.compressed_length = read_uint()
        # padding
        dir_file.seek(4, 1)
        file_part.uncompressed_length = read_uint()
        # padding
        dir_file.seek(2, 1)

        return file_part

    def read_file_node(extension: str) -> FileNode:
        file_node = FileNode(read_string())

        file_node.CRC = read_uint()
        file_node.file_ext = extension
        while protected_peek(4)[:4].hex() != '0000ffff':
            file_node.file_parts.append(read_file_part())

        dir_file.seek(4, 1)
        return file_node

    def read_path_node(extension: str) -> DirectoryNode:
        path_node = DirectoryNode(read_string())

        while protected_peek(1)[0] != 0:
            path_node.children.append(read_file_node(extension))

        dir_file.seek(1, 1)
        return path_node

    def read_extension_node() -> DirectoryNode:
        extension_node = DirectoryNode(read_string())

        while protected_peek(1)[0] != 0:
            extension_node.children.append(read_path_node(extension_node.get_name()))

        dir_file.seek(1, 1)
        return extension_node


    try:
        dir_file = open(dir_vpk_file, 'rb')
    except:
        raise ValueError(ERROR.DIRECTORY_OPEN)

    # magic
    if protected_read(4).hex() != '3412aa55':
        raise ValueError(ERROR.DIRECTORY_NO_MAGIC)
    # version
    if protected_read(4).hex() != '02000300':
        print_warning('vpk.dir version is not 2.3; may fail to read')
    directory_length = read_uint()
    # padding
    dir_file.seek(4, 1)

    root_node = DirectoryNode('root', True)
    while protected_peek(1)[0] != 0:
        root_node.children.append(read_extension_node())

    return root_node


########################################
# Helpers ##############################
########################################
def directory_checksum(native_directory: DirectoryNode) -> str:
    # extension_list is List[List[str]]
    # extensions is List[str]
    # extensions is every element from extension_list appended together
    # this is similar for paths and files

    extension_list = []
    for extension in native_directory.children:
        path_list = []
        for path in extension.children:
            file_list = []
            for file in path.children:
                file_list.append([file.get_name(),
                              str(file.CRC),
                              str(file.get_decompressed_length())])
            file_list.sort()
            files = [' ' if path.is_root else path.get_name()]
            for file in file_list:
                files.extend(file)
            path_list.append(files)
        path_list.sort()
        paths = [extension.get_name()]
        for path in path_list:
            paths.extend(path)
        extension_list.append(paths)
    extension_list.sort()
    extensions = []
    for extension in extension_list:
        extensions.extend(extension)
    return hashlib.sha1(bytearray(''.join(extensions), 'ascii')).hexdigest()


def decompress_file(file: FileNode, unindexed_raw_vpk_file: str):
    # this function interfaces with lzham and uses a special file format described in vpk_help.txt
    def get_indexed_vpk_name(archive_id: int) -> str:
        return unindexed_raw_vpk_file\
               + '0'*(3 - len(str(archive_id)))\
               + str(archive_id) + '.vpk'

    def write_temp_file():
        try:
            temp_file = open(TEMP_FILE, 'wb')
        except:
            raise ValueError(ERROR.TEMP_CREATE)
        temp_file.write(struct.pack('<I', len(file.file_parts)))
        last_archive_index = -1
        last_vpk = None
        for part in file.file_parts:
            if last_archive_index != part.archive_index:
                if last_archive_index != -1:
                    last_vpk.close()
                try:
                    last_vpk = open(get_indexed_vpk_name(part.archive_index), 'rb')
                except:
                    raise ValueError(ERROR.VPK_OPEN)
                last_archive_index = part.archive_index
            last_vpk.seek(part.offset)
            if part.compressed_length == part.uncompressed_length:
                temp_file.write(struct.pack('<II', 0, part.compressed_length))
            else:
                temp_file.write(struct.pack('<I', part.compressed_length))
            temp_file.write(last_vpk.read(part.compressed_length))
        temp_file.write(struct.pack('<I', file.CRC))
        # account for file with no parts, even though they shouldn't exist
        if last_archive_index != -1:
            last_vpk.close()
        temp_file.close()

    write_temp_file()
    if os.system(LZHAM_EXE + ' '
                 + 'd '
                 + TEMP_FILE + ' '
                 + RESULT_FILE):
        raise ValueError(ERROR.FILE_DECOMPRESS)


def cleanup_temporary_files():
    for file in [TEMP_FILE, RESULT_FILE]:
        if os.path.exists(file):
            try:
                os.remove(file)
            except:
                raise ValueError(ERROR.TEMP_CLEANUP)


def find_node_in_directory(node_path: str, directory: DirectoryNode, native: bool) -> DirectoryNode:
    if node_path.startswith('/'):
        node_path = node_path[1:]
    if node_path.endswith('/'):
        node_path = node_path[:-1]
    if not node_path:
        return directory

    search_terms = []
    if native:
        parts = node_path.split('/')
        if len(parts) == 1:
            search_terms = parts
        elif len(parts) == 2 or '.' not in parts[-1]:
            search_terms = [parts[0], '/'.join(parts[1:])]
        else:
            search_terms = [parts[0], '/'.join(parts[1:-1]), parts[-1]]
    else:
        search_terms = node_path.split('/')

    current_node = directory
    for term in search_terms:
        if current_node.is_file():
            raise ValueError(ERROR.PATH_FILE_WITHIN_FILE)
        found_node = False
        for child in current_node.children:
            if child.get_name() == term:
                found_node = True
                current_node = child
                break
        if not found_node:
            raise ValueError(ERROR.PATH_DOES_NOT_EXIST)

    return current_node


def get_vpk_file_base(dir_vpk_file: str) -> str:
    dir, file = os.path.split(dir_vpk_file)
    if dir_vpk_file.find('client') == -1 or dir_vpk_file.find('dir.vpk') == -1:
        raise ValueError(ERROR.DIRECTORY_NAME_UNRECOGNIZED)
    base_name = dir_vpk_file[dir_vpk_file.find('client'):-len('dir.vpk')]
    if not base_name:
        raise ValueError(ERROR.DIRECTORY_NAME_UNRECOGNIZED)
    return os.path.join(dir, base_name)


########################################
# Commands #############################
########################################
def list_command(dir_vpk_file: str, native: bool):
    try:
        native_directory = load_vpk_directory(dir_vpk_file)
    except Exception as e:
        print_error(str(e))
        return

    print_accent('\n\n════╣ Directory ╠════')
    if native:
        printing_directory = native_directory
    else:
        printing_directory = native_directory.to_path_directory()
    printing_directory.print()

    print_accent('\n\n════╣ Stats ╠════')
    extensions: Dict[str, int] = {}
    compressed_size = 0
    decompressed_size = 0
    flat_directory = native_directory.to_flat_directory()
    number_of_files = len(flat_directory.children)
    for file in flat_directory.children:
        extension = file.get_name().split('.')[-1]
        if extension in extensions:
            extensions[extension] += 1
        else:
            extensions[extension] = 1
        compressed_size += file.get_compressed_length()
        decompressed_size += file.get_decompressed_length()
    print(number_of_files, 'files')
    print(len(extensions), 'file types')
    print(compressed_size, 'compressed')
    print(decompressed_size, 'decompressed')
    print(str(compressed_size / decompressed_size * 100) + '% compression ratio\n')
    for extension in extensions:
        print(extension + ':', extensions[extension], 'files')


def search_command(dir_vpk_file: str, search_term: str, native: bool):
    def remove_children_not_containing(node: DirectoryNode, search_term: str):
        children_to_remove = []
        for child in node.children:
            if child.is_file():
                if search_term not in child.get_name():
                    children_to_remove.append(child)
            else:
                remove_children_not_containing(child, search_term)
                if search_term not in child.get_name() and not child.children:
                    children_to_remove.append(child)
        for child in children_to_remove:
            node.children.remove(child)

    try:
        native_directory = load_vpk_directory(dir_vpk_file)
    except Exception as e:
        print_error(str(e))
        return

    if native:
        search_directory = native_directory
    else:
        search_directory = native_directory.to_path_directory()

    remove_children_not_containing(search_directory, search_term)

    if not search_directory.children:
        print('No items found')
    else:
        search_directory.print()


def unpack_command(dir_vpk_file: str, path: str, native: bool, output_path: str):
    def unpack_directory_node(directory_node: DirectoryNode, output_path: str):
        this_directory = '' if directory_node.is_directory_root else directory_node.get_name()
        output_path = os.path.join(output_path, this_directory)
        try:
            os.makedirs(output_path, exist_ok=True)
        except:
            raise ValueError(ERROR.UNPACK_DIRECTORY_CREATE)

        for child in directory_node.children:
            if child.is_file():
                try:
                    decompress_file(child, vpk_base)
                    shutil.move(RESULT_FILE, os.path.join(output_path, child.get_name()))
                except Exception as e:
                    print_error(str(e))
            else:
                unpack_directory_node(child, output_path)

    if not path:
        path = ''

    try:
        native_directory = load_vpk_directory(dir_vpk_file)
    except Exception as e:
        print_error(str(e))
        return

    if native:
        using_directory = native_directory
    else:
        using_directory = native_directory.to_path_directory()

    try:
        base_node = find_node_in_directory(path, using_directory, native)
    except Exception as e:
        print_error(str(e))
        return

    try:
        vpk_base = get_vpk_file_base(dir_vpk_file)
    except Exception as e:
        print_error(str(e))
        return

    if not output_path:
        output_path = SCRIPT_DIR
    output_path = os.path.join(output_path, os.path.split(vpk_base)[-1])
    print(output_path)

    unpack_directory_node(base_node, output_path)

    try:
        cleanup_temporary_files()
    except Exception as e:
        print_error(str(e))


def checksum_command(dir_vpk_file: str):
    try:
        native_directory = load_vpk_directory(dir_vpk_file)
    except Exception as e:
        print_error(str(e))
        return

    print(directory_checksum(native_directory))


def verify_command(dir_vpk_file: str):
    def print_no_verify():
        print('Unable to verify this vpk')

    try:
        native_directory = load_vpk_directory(dir_vpk_file)
    except Exception as e:
        print_error(str(e))
        print_no_verify()
        return

    flat_directory = native_directory.to_flat_directory()
    try:
        vpk_file_base = get_vpk_file_base()
    except Exception as e:
        print_error(str(e))
        print_no_verify()
        return

    for file in flat_directory.children:
        try:
            decompress_file(file, vpk_file_base)
        except Exception as e:
            print_error(file.get_name() + ' ' + str(e))
            print_no_verify()
            return

    try:
        cleanup_temporary_files()
    except Exception as e:
        print_error(str(e))

    print_success('vpk verified')


def test_command(dir_vpk_file: str, path: str, native: bool):
    try:
        native_directory = load_vpk_directory(dir_vpk_file)
    except Exception as e:
        print_error(str(e))
        return

    if native:
        using_directory = native_directory
    else:
        using_directory = native_directory.to_path_directory()

    found = find_node_in_directory(path, using_directory, native)
    found.print()


def parse_args():
    o_help = 'Sets the output directory (script directory by default)'
    n_help = 'Uses the vpk native path format: extension/full_path/file_name'
    p_help = 'Specifies a path within the vpk to operate on'

    parser = argparse.ArgumentParser(prog='vpk')
    parser.add_argument('--version', action='version', version='vpk tool 0.2\nCopyright Noah Olson')
    subparsers = parser.add_subparsers(dest='command')

    list_subparser = subparsers.add_parser('list', help='Prints the directory structure of a vpk file')
    list_subparser.add_argument('dir_vpk_file')
    list_subparser.add_argument('-n', '--native', action='store_true', help=n_help)

    search_subparser = subparsers.add_parser('search', help='Searches for a file or path')
    search_subparser.add_argument('dir_vpk_file')
    search_subparser.add_argument('search_term')
    search_subparser.add_argument('-n', '--native', action='store_true', help=n_help)

    unpack_subparser = subparsers.add_parser('unpack', help='Unpacks a vpk file or part of a vpk file')
    unpack_subparser.add_argument('dir_vpk_file')
    unpack_subparser.add_argument('-p', '--path', help=p_help)
    unpack_subparser.add_argument('-n', '--native', action='store_true', help=n_help)
    unpack_subparser.add_argument('-o', '--output', help=o_help)

    pack_subparser = subparsers.add_parser('pack', help='Packs a folder into a vpk')
    # Todo

    add_subparser = subparsers.add_parser('add', help='Adds a file or folder to a vpk file')
    # Todo

    remove_subparser = subparsers.add_parser('remove', help='Removes a path from a vpk file')
    # Todo

    checksum_subparser = subparsers.add_parser('checksum', help='Calculates the checksum of a dir.vpk file')
    checksum_subparser.add_argument('dir_vpk_file')

    verify_subparser = subparsers.add_parser('verify', help='Checks that a vpk file is not corrupt')
    verify_subparser.add_argument('dir_vpk_file')

    diff_print_subparser = subparsers.add_parser('diff-print', help='Prints the differences between two vpk files')
    # Todo

    diff_subparser = subparsers.add_parser('diff', help='Creates a patch that can change base vpk to modded vpk with patch')
    # Todo

    patch_subparser = subparsers.add_parser('patch', help='Applies a patch to a vpk file')
    # Todo

    test_subparser = subparsers.add_parser('test', help='For testing only')
    test_subparser.add_argument('file')
    test_subparser.add_argument('path')
    test_subparser.add_argument('-n', '--native', action='store_true', help=n_help)


    args = parser.parse_args()

    if args.command == 'list':
        list_command(args.dir_vpk_file,
                     args.native)
    elif args.command == 'search':
        search_command(args.dir_vpk_file,
                       args.search_term,
                       args.native)
    elif args.command == 'unpack':
        unpack_command(args.dir_vpk_file,
                       args.path,
                       args.native,
                       args.output)
    elif args.command == 'pack':
        pass
    elif args.command == 'add':
        pass
    elif args.command == 'remove':
        pass
    elif args.command == 'checksum':
        checksum_command(args.dir_vpk_file)
    elif args.command == 'verify':
        verify_command(args.dir_vpk_file)
    elif args.command == 'diff-print':
        pass
    elif args.command == 'diff':
        pass
    elif args.command == 'patch':
        pass
    elif args.command == 'test':
        test_command(args.file,
                     args.path,
                     args.native)


if __name__ == '__main__':
    parse_args()
