# imports props from the game lump of a bsp
# trying to import everything will probably not work
# a custom map viewer will most likely be required if a whole map is to be loaded

import sys
import struct
import os
import math
import bpy
from mathutils import Vector



class Instance:
    def __init__(self, id, origin, rotation):
        self.id = id
        self.origin = origin
        self.rotation = rotation

    def print(self):
        print(self.id, self.origin, self.rotation)


excluded_models_file = ''
model_directory = ''
sprp_file = ''


excluded_models = []
names_file = open(excluded_models_file, 'r')
for line in names_file.readlines():
    excluded_models.append(line[:-1])
names_file.close()



#in_file = open(sys.argv[1], 'rb')
in_file = open(sprp_file, 'rb')
num_game_lumps = struct.unpack('<I', in_file.read(4))[0]
print(num_game_lumps, 'lumps')
(id, flags, version, offset, length) = struct.unpack('<IHHII', in_file.read(16))

num_prop_types = struct.unpack('<I', in_file.read(4))[0]
print(num_prop_types, 'types')
prop_names = []
for i in range(num_prop_types):
    prop_name = in_file.read(128).decode('ascii')
    prop_name = prop_name[:prop_name.find('\0')]
    prop_names.append(prop_name)


    
num_prop_instances = struct.unpack('<I8x', in_file.read(12))[0]
print(num_prop_instances, 'instances')
prop_instances_by_id = []
for i in range(num_prop_types):
    prop_instances_by_id.append([])
for i in range(num_prop_instances):
    origin = struct.unpack('<3f', in_file.read(12))
    rotation = struct.unpack('<3f', in_file.read(12))
    scale = struct.unpack('<f', in_file.read(4))[0]
    id = struct.unpack('<H34x', in_file.read(36))[0]
    prop_instances_by_id[id].append(Instance(id, origin, rotation))
    
#for prop in prop_instances_by_id[95]:
#    prop.print()
#exit()
    
for i in range(num_prop_types):
#for i in range(20):
    prop_name = prop_names[i]
    prop_name = prop_name[prop_name.rfind('/') + 1:-5]
    if prop_name in excluded_models:
        continue
    
    print('loading', i, prop_name, 'with', len(prop_instances_by_id[i]), 'instances')
    file_path = os.path.join(os.path.join(model_directory, prop_name), prop_name + '_LOD0.semodel')
    prior_objects = [object.name for object in bpy.context.scene.objects]
    bpy.ops.import_scene.semodel(filepath=file_path)
    after_import_objects = [object.name for object in bpy.context.scene.objects]
    new_objects = set(after_import_objects) - set(prior_objects)
    
    continue
    
    mesh_object = None
    for object in new_objects:
        if bpy.data.objects[object].type == 'MESH':
            mesh_object = object
        else:
            bpy.ops.object.select_all(action='DESELECT')
            bpy.data.objects[object].select = True
            bpy.ops.object.delete()
    if not mesh_object:
        print(prop_name, 'did not have a mesh')
        continue
    
    bpy.context.scene.objects.active = bpy.data.objects[mesh_object]
    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects[mesh_object].select = True
    for inst in prop_instances_by_id[i]:
        bpy.ops.object.duplicate(linked=True)
        
        bpy.context.active_object.location = Vector(inst.origin)
        bpy.context.active_object.rotation_euler = Vector((math.radians(inst.rotation[2]), math.radians(inst.rotation[0]), math.radians(inst.rotation[1])))
        print(inst.rotation)
    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects[mesh_object].select = True
    bpy.ops.object.delete()
    #break
    # for each instance
        # duplicate mesh
        # translate, rotate
    # delete mesh
    






#for inst in prop_instances:
#    inst.print()

#used_prop_names = set()
#for inst in prop_instances:
#    prop_name = prop_names[inst.id]
#    used_prop_names.add(prop_name[prop_name.rfind('/') + 1:-5])
#for name in sorted(used_prop_names):
#    print(name)
