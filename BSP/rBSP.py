import struct
import sys
from typing import List, Tuple
import os


def read_lumps(file) -> List[Tuple[int, int, int, int]]:
    # magic
    if file.read(4).hex() != '72425350':
        print('magic is invalid: quitting')
        sys.exit()
    # bsp version
    if file.read(4).hex() != '30000000':
        print('warning: version is unrecognized')
    # revision
    file.read(4)
    # unknown
    file.read(4)
    # lumps
    lumps = []
    for i in range(128):
        lumps.append(struct.unpack('<4I', file.read(16)))
    return lumps


if len(sys.argv) != 3:
    print("missing arguments; run with:\n"
          "list {filename} or\n"
          "extract {filename}")
    sys.exit()
try:
    file = open(sys.argv[2], 'rb')
except:
    print('could not open file: quitting')
    sys.exit()
lumps = read_lumps(file)

if sys.argv[1] == 'extract':
    vertex_array: List[Tuple[float, float, float]] = []
    file.seek(lumps[3][0])
    for i in range(lumps[3][1] // 3):
        vertex_array.append(struct.unpack('<3f', file.read(12)))

    index_array: List[int] = []
    file.seek(lumps[79][0])
    for i in range(lumps[79][1] // 2):
        index_array.append(struct.unpack('<H', file.read(2))[0])

    normal_array: List[Tuple[float, float, float]] = []
    file.seek(lumps[30][0])
    for i in range(lumps[30][1] // 12):
        normal_array.append(struct.unpack('<3f', file.read(12)))

    # vertex index, normal index
    bump_lit_vertex_array: List[Tuple[int, int]] = []
    file.seek(lumps[73][0])
    for i in range(lumps[73][1] // 32):
        bump_lit_vertex_array.append(struct.unpack('<2I24x', file.read(32)))

    material_sort_array = []
    file.seek(lumps[82][0])
    for i in range(lumps[82][1] // 12):
        material_sort_array.append(struct.unpack('<8xI', file.read(12))[0])

    # index_array start, # triangles, material_sort_array index
    mesh_array: List[Tuple[int, int, int]] = []
    file.seek(lumps[80][0])
    for i in range(lumps[80][1] // 28):
        data = file.read(28)
        mesh_type = 0
        type_int = struct.unpack('<24xI', data)[0]
        if type_int & 0x400:
            mesh_type |= 1
        if type_int & 0x200:
            mesh_type |= 2
        if mesh_type == 2:
            mesh_array.append(struct.unpack('<IH16xH4x', data))

    total_tris = 0
    final_index_array: List[List[Tuple[int, int]]] = []
    for mesh in mesh_array:
        total_tris += mesh[1]
        faces: List[Tuple[int, int]] = []
        for i in range(mesh[1] * 3):
            vertex = bump_lit_vertex_array[material_sort_array[mesh[2]] + index_array[mesh[0] + i]]
            faces.append((vertex[0], vertex[1]))
        final_index_array.append(faces)

    out_file = open(os.path.splitext(os.path.split(sys.argv[2])[-1])[0] + '.stl', 'wb')
    out_file.write(bytes.fromhex('00' * 80))
    out_file.write(struct.pack('<I', total_tris))
    for i in range(len(final_index_array)):
        faces = final_index_array[i]

        for j in range(len(faces) // 3):
            v1_index = faces[j * 3][0]
            v2_index = faces[j * 3 + 1][0]
            v3_index = faces[j * 3 + 2][0]
            v1 = vertex_array[v1_index]
            v2 = vertex_array[v2_index]
            v3 = vertex_array[v3_index]

            # Todo: if vert normals are normal to the vert and not the plane, maybe recalculate them
            normal_index = faces[j * 3][1]
            n = normal_array[normal_index]

            out_file.write(struct.pack('<12fH',
                                       n[0],  n[1],  n[2],
                                       v1[0], v1[1], v1[2],
                                       v2[0], v2[1], v2[2],
                                       v3[0], v3[1], v3[2],
                                       0))
    out_file.close()

if sys.argv[1] == 'list':
    lump_names = [
        'LUMP_ENTITIES',
        'LUMP_PLANES',
        'LUMP_TEXDATA',
        'LUMP_VERTEXES',
        'LUMP_DEPRECATED_4',
        'LUMP_DEPRECATED_5',
        'LUMP_6_UNKNOWN',
        'LUMP_7_UNKNOWN',
        'LUMP_8_UNKNOWN',
        'LUMP_9_UNKNOWN',
        'LUMP_DEPRECATED_10',
        'LUMP_11_UNKNOWN',
        'LUMP_12_UNKNOWN',
        'LUMP_13_UNKNOWN',
        'LUMP_MODELS',
        'LUMP_15_UNKNOWN',
        'LUMP_DEPRECATED_16',
        'LUMP_17_UNKNOWN',
        'LUMP_18_UNKNOWN',
        'LUMP_19_UNKNOWN',
        'LUMP_DEPRECATED_20',
        'LUMP_DEPRECATED_21',
        'LUMP_DEPRECATED_22',
        'LUMP_DEPRECATED_23',
        'LUMP_ENTITYPARTITIONS',
        'LUMP_25_UNKNOWN',
        'LUMP_26_UNKNOWN',
        'LUMP_27_UNKNOWN',
        'LUMP_28_UNKNOWN',
        'LUMP_PHYSCOLLIDE',
        'LUMP_VERTNORMALS',
        'LUMP_31_UNKNOWN',
        'LUMP_32_UNKNOWN',
        'LUMP_33_UNKNOWN',
        'LUMP_34_UNKNOWN',
        'LUMP_GAME_LUMP',
        'LUMP_LEAFWATERDATA',
        'LUMP_37_UNKNOWN',
        'LUMP_38_UNKNOWN',
        'LUMP_39_UNKNOWN',
        'LUMP_PAKFILE',
        'LUMP_DEPRECATED_41',
        'LUMP_CUBEMAPS',
        'LUMP_TEXDATA_STRING_DATA',
        'LUMP_TEXDATA_STRING_TABLE',
        'LUMP_DEPRECATED_46',
        'LUMP_46_UNKNOWN',
        'LUMP_47_UNKNOWN',
        'LUMP_48_UNKNOWN',
        'LUMP_49_UNKNOWN',
        'LUMP_50_UNKNOWN',
        'LUMP_51_UNKNOWN',
        'LUMP_52_UNKNOWN',
        'LUMP_DEPRECATED_53',
        'LUMP_WORLDLIGHTS_HDR',
        'LUMP_55_UNKNOWN',
        'LUMP_56_UNKNOWN',
        'LUMP_57_UNKNOWN',
        'LUMP_58_UNKNOWN',
        'LUMP_DEPRECATED_59',
        'LUMP_60_UNKNOWN',
        'LUMP_61_UNKNOWN',
        'LUMP_PHYSLEVEL',
        'LUMP_63_UNKNOWN',
        'LUMP_64_UNKNOWN',
        'LUMP_65_UNKNOWN',
        'LUMP_TRICOLL_TRIS',
        'LUMP_67_UNKNOWN',
        'LUMP_TRICOLL_NODES',
        'LUMP_TRICOLL_HEADERS',
        'LUMP_PHYSTRIS',
        'LUMP_VERTS_UNLIT',
        'LUMP_VERTS_LIT_FLAT',
        'LUMP_VERTS_LIT_BUMP',
        'LUMP_VERTS_UNLIT_TS',
        'LUMP_VERTS_BLINN_PHONG',
        'LUMP_VERTS_RESERVED_5',
        'LUMP_VERTS_RESERVED_6',
        'LUMP_VERTS_RESERVED_7',
        'LUMP_MESH_INDICES',
        'LUMP_MESHES',
        'LUMP_MESH_BOUNDS',
        'LUMP_MATERIAL_SORT',
        'LUMP_LIGHTMAP_HEADERS',
        'LUMP_LIGHTMAP_DATA_DXT5',
        'LUMP_CM_GRID',
        'LUMP_CM_GRIDCELLS',
        'LUMP_CM_GEO_SETS',
        'LUMP_CM_GEO_SET_BOUNDS',
        'LUMP_CM_PRIMS',
        'LUMP_CM_PRIM_BOUNDS',
        'LUMP_CM_UNIQUE_CONTENTS',
        'LUMP_CM_BRUSHES',
        'LUMP_CM_BRUSH_SIDE_PLANE_OFFSETS',
        'LUMP_CM_BRUSH_SIDE_PROPS',
        'LUMP_CM_BRUSH_TEX_VECS',
        'LUMP_TRICOLL_BEVEL_STARTS',
        'LUMP_TRICOLL_BEVEL_INDEXES',
        'LUMP_LIGHTMAP_DATA_SKY',
        'LUMP_CSM_AABB_NODES',
        'LUMP_CSM_OBJ_REFS',
        'LUMP_LIGHTPROBES',
        'LUMP_STATIC_PROP_LIGHTPROBE_INDEX',
        'LUMP_LIGHTPROBETREE',
        'LUMP_LIGHTPROBEREFS',
        'LUMP_LIGHTMAP_DATA_REAL_TIME_LIGHTS',
        'LUMP_CELL_BSP_NODES',
        'LUMP_CELLS',
        'LUMP_PORTALS',
        'LUMP_PORTAL_VERTS',
        'LUMP_PORTAL_EDGES',
        'LUMP_PORTAL_VERT_EDGES',
        'LUMP_PORTAL_VERT_REFS',
        'LUMP_PORTAL_EDGE_REFS',
        'LUMP_PORTAL_EDGE_ISECT_EDGE',
        'LUMP_PORTAL_EDGE_ISECT_AT_VERT',
        'LUMP_PORTAL_EDGE_ISECT_HEADER',
        'LUMP_OCCLUSIONMESH_VERTS',
        'LUMP_OCCLUSIONMESH_INDICES',
        'LUMP_CELL_AABB_NODES',
        'LUMP_OBJ_REFS',
        'LUMP_OBJ_REF_BOUNDS',
        'LUMP_DEPRECATED_122',
        'LUMP_LEVEL_INFO',
        'LUMP_SHADOW_MESH_OPAQUE_VERTS',
        'LUMP_SHADOW_MESH_ALPHA_VERTS',
        'LUMP_SHADOW_MESH_INDICES',
        'LUMP_SHADOW_MESH_MESHES'
    ]
    longest_name_length = 35

    print('lump name | offset | length | version | id')
    for i in range(128):
        print(lump_names[i] + ' ' * (longest_name_length - len(lump_names[i])), lumps[i][0], lumps[i][1], lumps[i][2], lumps[i][3])

file.close()
