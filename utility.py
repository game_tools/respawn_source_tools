import os
import sys
from typing import List


def partition_file_by_offsets(file: str, offsets: List[int]):
    file_base = os.path.splitext(file)[0]
    file = open(file, 'rb')
    offsets.sort()
    for i in range(len(offsets) - 1):
        length = offsets[i + 1] - offsets[i]
        if length:
            out_file = open(file_base + '_' + str(offsets[i]), 'wb')
            file.seek(offsets[i], 0)
            out_file.write(file.read(length))
            out_file.close()
    file.seek(offsets[-1], 0)
    if file.peek(1):
        out_file = open(file_base + '_' + str(offsets[-1]), 'wb')
        while file.peek(1):
            out_file.write(file.read(1))
        out_file.close()
    file.close()


# simple diff, prints positions that are different
def diff(file1: str, file2: str):
    file1 = open(file1, 'rb')
    file2 = open(file2, 'rb')
    while file1.peek(1) and file2.peek(1):
        if file1.read(1) != file2.read(1):
            print(file1.tell())
    file1.close()
    file2.close()