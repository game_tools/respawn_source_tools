

the first 128 bytes is a header. this should be the same for all rpaks
struct header {
    u32 magic    // 0x5250616B
    u32 version  // not sure how this is formatted; it is currently 0x08000001
    u64 rpak_type
    u8[8] unknown
    u32 file_size_on_disk
    u8[50] unknown
    u16 num_struct_a
    u32 pad
    u32 num_struct_b
    u32 num_entries
    u32 num_struct_c
    u32 num_struct_d
    u8[28] unknown
}

the rest of the file is compressed. the rest of this information
refers to the compressed data, which will be referred to as the directory


// There are a number of variants, which are identified with at least the rpak_type
// member from the header (and possibly some other information).
// The variants seem specific to the type of files they hold.
// It seems most or all variants have a, b, entry, c, d structs, but
// they are sometimes different sizes.
// some of this is not correct

// entry seems to be the same across all variants
struct entry {
    u64 uid
    u64 unused // 0
    u32 index_unknown
    u32 unknown
    u64 unknown // usually 0xff ff ff ff 00 00 00 00
    u8[16] unused // usually 0xff
    u16
    u16
    u32
    u32
    u32
    u32 type_1
    u32 type_2
    u32 type_3 // seems to be a version
    u32 type_string
}

---------------------------------------------------------------------
rpak of type 0x21ea6ef576f3d601 with multiple entries WITHOUT STARPAK
ui
startup
effects

struct directory {
    u8[96] unknown
    
    a[num_struct_a] a
    b[num_struct_b] b
    entry[num_entries] entries
    c[num_struct_c] c
    d[num_struct_d] d
}

struct a {
    u8[12] unknown
}

struct b {
    u8[8] unknown
}

struct c {
    u8[8] unknown
}

struct d {
    u8[4] unknown
}

-----------------------------------------------
rpak of type 0x4ffb41947af3d601 WITHOUT STARPAK
loadscreens
comics
ui_lobby
gcard_frames

struct directory {
    u8[96] unknown
    u8[] unknown
    b_2[num_struct_b] b_2
    entry[num_entries] entries
    c_2[num_struct_c] c_2
    d_2[num_struct_d] d_2
}

struct b_2 {
    u8[8] unknown
}

struct c_2 {
    u8[12] unknown
}

struct d_2 { // for textures
    u64 uid
    u64 unknown
    u16 width
    u16 height
    u32 unknown
    u64 data_length
    u8[24] unknown
}

------------------------------------------------------------
rpak of type 0x4ffb41947af3d601 WITH STARPAK AND OPT.STARPAK
almost all charms

struct directory {
    
    b_3[num_struct_b] b
    entry[num_entries] entries
    c_3[num_struct_c] c
    d_3[num_struct_d] d
}

struct c_3 {
    u8[8] unknown
}

struct d_3 {
    u8[4] unknown
}
