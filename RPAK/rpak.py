import sys
import struct

in_file = open(sys.argv[1], 'rb')
# out_file = open(sys.argv[1] + '.data', 'wb')

in_file.seek(4+4+16+4+50)

num_a, num_b, num_entries, num_c, num_d = struct.unpack('<H4x4I28x', in_file.read(50))
print('a:', num_a, 'b:', num_b, 'entries:', num_entries, 'c:', num_c, 'd:', num_d)

# in_file.seek(num_a * 12 + num_b * 8 + num_entries * 80 + num_c * 8 + num_d * 4 + 96, 1)
# out_file.write(in_file.read())

