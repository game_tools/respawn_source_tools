import sys
import struct
from typing import List, Tuple


def convert(file: str):
    in_file = open(file, 'rb')
    directory = struct_loader.StructObject('rmdl.struct')
    directory.load_data_from_file(file)
    verts_offset = directory.fields['vertexes1']['offset']
    num_verts = directory.fields['vertexes1']['length'] // 20
    in_file.seek(verts_offset)

    out_file = open(file + '.obj', 'w')

    for i in range(num_verts):
        [x, y, z] = struct.unpack('<fff8x', in_file.read(20))
        x /= 1000
        y /= 1000
        z /= 1000
        out_file.write('v ' + str(x) + ' ' + str(y) + ' ' + str(z) + '\n')

    out_file.close()
    in_file.close()


def print_columns(rows: List[Tuple]):
    number_of_columns = len(rows[0])
    lengths = []
    for i in range(number_of_columns):
        lengths.append(0)
        for row in rows:
            if len(row[i]) > lengths[i]:
                lengths[i] = len(row[i])
    for row in rows:
        for i in range(number_of_columns):
            print(' ' * (lengths[i] - len(row[i])), row[i], end='')
        print()


def list_sections(file: str):
    sections = [
        # the length of each section entry sometimes means the number of elements
        # and other times, the number of bytes
        # section name, length to elements divisor, element size
        ('unknown1', -1, -1),
        ('unknown2', 1, 72),
        ('unknown3', -1, -1),
        ('vertexes1', 20, 20),
        ('unknown5', -1, -1),
        ('meshes', 1, 48),
        ('unknown7', 1, 8),
        ('vertexes2', -1, -1),
        ('unknown9', 1, 35),
        ('unused1', -1, -1),
        ('unused2', -1, -1),
        ('unused3', -1, -1),
        ('unused4', -1, -1),
    ]
    in_file = open(file, 'rb')
    # skip header
    in_file.seek(16)
    rows = [('section name', 'offset', 'number of elements', 'element length')]
    for i in range(len(sections)):
        offset, length = struct.unpack('<QQ', in_file.read(16))
        rows.append((sections[i][0], str(offset), str(length // sections[i][1]), str(sections[i][2])))
    print_columns(rows)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Error: 2 arguments required: mode and file')
        sys.exit()
    if sys.argv[1] == 'convert':
        convert(sys.argv[2])
    elif sys.argv[1] == 'list':
        list_sections(sys.argv[2])
    else:
        print('Error: unknown mode')
        sys.exit()
