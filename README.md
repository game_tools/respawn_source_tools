# Despawn Source Tools
A collection of tools for extracting game data from Respawn's custom version of Source Engine.

These tools were built for Apex Legends. Some formats have changed between Titanfall | 2
and Apex, but some remain the same.

## Tools

- [ ] VPK  
  vpk.py is the vpk tool. Use -h to see usage or look at vpk_help.txt.
  - [x] Directory Listing
  - [x] Directory Searching
  - [ ] Folder Packing
  - [x] VPK Unpacking
  - [ ] Adding files to VPK
  - [ ] Removing files from VPK
  - [x] Directory Checksumming
  - [x] VPK Verifying
  - [ ] VPK Merging
  - [ ] Directory Diff Printing
  - [ ] VPK Diffing
  - [ ] VPK Patching
  
- [x] VFONT  
  Decrypt VFONTs with vfont_extractor.exe.  
  Arg1: encrypted vfont file  
  Arg2: destination file  
  The result is a TrueType Font.

- [ ] raw_hdr  
  3D LUT files, new in Apex
  
- [ ] VTF  
  One new image data format
  
- [ ] RPAK  
  - [x] Decompression of rpaks
  - [ ] Parsing of rpak directory
  
- [ ] rBSP
  